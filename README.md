# SuperDisco
This is a project dedicated to separating (co)variance of a supervised PLS analysis into common and distinct compartments between blocks  
The work is led and coordinated by Carl Brunius <carl.brunius@chalmers.se>  
Department of Biology and Biological Engineering, Chalmers University of Technology  
Department of Molecular Sciences, Swedish University of Agricultural Sciences  

## Installation
Install `devtools` to be able to install packages from Git repositories.

Install `SuperDISCO` package by:

`devtools::install_git("https://gitlab.com/CarlBrunius/SuperDISCO.git")`

## Data and script
Upon installation, there will be a `Workflow` folder in the `SuperDISCO` folder of your R library.  
In that folder, you will find an R script `Workflow.R` which calls in example data and performs a DISCO-PLS analysis.

## References
- *Petters P, Landberg R and Brunius C. DISCO-PLS for enhanced interpretability of supervised multiblock data analysis. Submitted.*

## Version history
Version | Date | Description
:------ | :--- | :----------
0.1.1 | 2017-06-20 | Updated workflow to perform pls with mixOmics instead of pls package.
0.1.0 | 2017-03-15 | Functionalised package with DISCO-PLS, plotSD and other support functions.
