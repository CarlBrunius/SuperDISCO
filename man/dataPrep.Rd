% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/DISCO_Functions.R
\name{dataPrep}
\alias{dataPrep}
\title{Prepare data for SuperDISCO}
\usage{
dataPrep(inData, Names, center = T, scale = T)
}
\arguments{
\item{inData}{List with original data (list elements with rows=observations and columns=variables)}

\item{Names}{Names of the list elements (data types, e.g. miRNA, mRNA, 16S, metabolomics...)}

\item{center}{Boolean for whether to center data (defaults to TRUE)}

\item{scale}{Boolean for whether to autoscale data within blocks (defaults to TRUE)}
}
\value{
List of pre-processed data in format for SuperDISCO
}
\description{
Adapted from STATegRa package: Check number of observations; Remove constant variables; Scale & center (if selected)
}

